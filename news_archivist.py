﻿
#-----Statement of Authorship----------------------------------------#
#
#  This is an individual assessment item.  By submitting this
#  code I agree that it represents my own work.  I am aware of
#  the University rule that a student must not act in a manner
#  which constitutes academic dishonesty as stated and explained
#  in QUT's Manual of Policies and Procedures, Section C/5.3
#  "Academic Integrity" and Section E/2.1 "Student Code of Conduct".
#
#    Student no: n9942165
#    Student name: Alexander Ratcliff
#
#  NB: Files submitted without a completed copy of this statement
#  will not be marked.  Submitted files will be subjected to
#  software plagiarism analysis using the MoSS system
#  (http://theory.stanford.edu/~aiken/moss/).
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  News Archivist
#
#  In this task you will combine your knowledge of HTMl/XML mark-up
#  languages with your skills in Python scripting, pattern matching
#  and Graphical User Interface development to produce a useful
#  application for maintaining and displaying archived news or
#  current affairs stories on a topic of your own choice.  See the
#  instruction sheet accompanying this file for full details.
#
#--------------------------------------------------------------------#



#-----Imported Functions---------------------------------------------#
#
# Below are various import statements that were used in our sample
# solution.  You should be able to complete this assignment using
# these functions only.

# Import the function for opening a web document given its URL.
from urllib.request import urlopen

# Import the function for finding all occurrences of a pattern
# defined via a regular expression, as well as the "multiline"
# and "dotall" flags.
from re import findall, MULTILINE, DOTALL

# A function for opening an HTML document in your operating
# system's default web browser. We have called the function
# "webopen" so that it isn't confused with the "open" function
# for writing/reading local text files.
from webbrowser import open as webopen

# An operating system-specific function for getting the current
# working directory/folder.  Use this function to create the
# full path name to your HTML document.
from os import getcwd, listdir

# An operating system-specific function for 'normalising' a
# path to a file to the path-naming conventions used on this
# computer.  Apply this function to the full name of your
# HTML document so that your program will work on any
# operating system.
from os.path import normpath, isfile
    
# Import the standard Tkinter GUI functions.
from tkinter import *

# Import the SQLite functions.
from sqlite3 import *

# Import the date and time function.
import datetime

#
#--------------------------------------------------------------------#



#-----Student's Solution---------------------------------------------#
#
# Put your solution at the end of this file.
#

# Name of the folder containing your archived web documents.  When
# you submit your solution you must include the web archive along with
# this Python program. The archive must contain one week's worth of
# downloaded HTML/XML documents. It must NOT include any other files,
# especially image files.
internet_archive = 'InternetArchive'


################ PUT YOUR SOLUTION HERE #################
### Button functions
## Download button
# URL of news page - top stories
url = 'http://www.skynews.com.au/news/top-stories.html'

# Get the current date
if int(time.strftime("%d")) < 10:
    # If the date is before the 10th, add a 0 in front of the date for
    # consistency
    cur_date = time.strftime("%B 0%d")
else:
    # Otherwise, leave as normal
    cur_date = time.strftime("%B %d")


# Function to be called upon pressing download button
def download_newspage():
    # Update the status bar
    status_bar['text'] = 'Now extract news from a date'
    archivist_window.update()
    
    # Get the file path for the downloaded file
    file_path = getcwd() + re.sub(' ', '', r'\InternetArchive\ ' + cur_date + '.html')
    print(cur_date , '      ', file_path)
    
    # Open the web document for reading
    web_page = urlopen(url)

    # Read its contents as a Unicode string
    web_page_contents = web_page.read().decode('UTF-8')

    # Write the contents to a text file (overwriting the file if it
    # already exists)
    html_file = open(file_path, 'w', encoding = 'UTF-8')
    html_file.write(web_page_contents)
    html_file.close()

    # Add date to listbox
    dates_listbox.insert(END, cur_date)

    # Disable button, so you can not attempt to download the page again
    download_button['state'] = 'disabled'

    if (log_events.get() == 1):
            event_logger('Downloaded news for date: ' + cur_date)



## Extract news button
def extract_news():
    # Check if anything is currently selected
    if(dates_listbox.curselection()):
        # Update the status bar
        status_bar['text'] = 'Now display the news'
        archivist_window.update()
        
        # Get the currently selected date
        extract_date = dates_listbox.get(dates_listbox.curselection())

        # Start the html code
        html_start ='''<!DOCTYPE html>
<html>
    <head>
        <title>{date} News Archive</title>
        <style>
            body {
                background-color: #b5fdff;
                text-align: center;
            }

            .main-content {
                width: 80%;
                left: 0;
                right: 0;
                margin: auto;
                background-color: #FFFFFF;
            }
            
            .header img {
                width: 50%;
                height: 50%;
            }
        </style>
    </head>

    <body>
        <div class="main-content">
            <div class="header">
                <h1>SkyNews Top News</h1>
                <h3>Top news stories from {date}</h3>
                <img src="https://i.pinimg.com/originals/52/fb/3d/52fb3dc91655e\
cc634691d6a843fd557.jpg">
                <h3>News Source: <a href= http://www.skynews.com.au/news/top-st\
ories.html>SkyNews</a></h3>
                <h4>Archivist: Alexander Ratcliff</h4>
            </div>'''

        html_end ='''
        </div>
    </body>
</html>'''

        html_start = html_start.replace('{date}', extract_date)
 
        # Get data from original html file
        print('extract news from:' + extract_date)
        original_file = open(getcwd() + '\\InternetArchive\\' + re.sub(' ', '', \
                                       extract_date) + '.html', 'r')
        original_content = original_file.read()
        original_file.close()

        # Extract the articles from the entire page content
        start_point = original_content.find('<div class="col-md-12 sub-category-\
list">')
        end_point = original_content.find('</article></div>')
        news_articles = original_content[start_point:end_point]

        # Complete the links in the articles
        news_articles = news_articles.replace('<a href="/news', '<a href="http:\
//www.skynews.com.au/news')
        
        # Write the first part of the new html document
        extract_file = open(getcwd() + r'\news_extract.html', 'w')
        extract_file.write(html_start)
        
        # Get the individual articles and write them into the html document
        cur_pos = 0
        for article in range(10):
            # Beginning of the article
            article_begin = '''
                <hr>
                <div class = "articles">
                    <h3>Article ''' + str(article + 1) + '''</h3>
                    '''
            # Get the image
            img_start = news_articles.find('<a href=', cur_pos)
            img_end = news_articles.find('</a>', img_start)
            img = news_articles[img_start:img_end+4]

            # Get the date it was posted
            numbers = findall(r'\d+', img)
            publish_date = numbers[2] + '/' + numbers[1] + '/' + numbers[0]
            date_comment = '<p>This article was published on: ' + publish_date \
                           + '</p>'

            # Get the title
            title_start = news_articles.find('<h3>', cur_pos)
            title_end = news_articles.find('</h3>', title_start)
            title = news_articles[title_start:title_end+5]
           
            # Get the article
            article_start = news_articles.find("<p class='article-intro'>", \
                                               cur_pos)
            article_end = news_articles.find('</p>', article_start)
            article = news_articles[article_start:article_end+4]
            cur_pos = article_end

            # Write the article in the document
            extract_file.write(article_begin)
            extract_file.write(title)
            extract_file.write(img)
            extract_file.write(article)
            extract_file.write(date_comment)
            extract_file.write('''
                    </div>
                    ''')
        
        # Finish the html document
        extract_file.write(html_end)
        extract_file.close()
        if (log_events.get() == 1):
            event_logger('Extracted news for date: ' + extract_date)
    else:
        popup_error('There is no date currently selected, please select one bef\
ore trying to extract news.')
        if (log_events.get() == 1):
            event_logger('Failed to extract news, because no date is selected')


## Display News button
def display_extracted():
    file_path = getcwd() + r'\news_extract.html'
    # Check the file exists before trying to open it
    if (isfile(file_path)):
        webopen(file_path)
        if (log_events.get() == 1):
            event_logger('Viewed extracted news')

    else:
        popup_error('You have no news already extracted, please extract some fi\
rst before trying to display it.')
        if (log_events.get() == 1):
            event_logger('Failed to display news, because nothing is extracted')
    
    

## Displays a popup box with an error message
def popup_error(message):
    # Create a new top level window on screen
    global popup_window
    popup_window = Toplevel()

    # Display message
    Label(popup_window, text = message).pack()

    # Button to close popup window
    Button(popup_window, text = 'Ok', command = close_popup).pack()



# Close the popup window when the button is pressed
def close_popup():
    popup_window.destroy()



# Part B - function to log events, when called
def event_logger(event):
    # Open the event log database
    eventlog_db = connect('event_log.db')
    db_view = eventlog_db.cursor()

    # Predefined statement too insert new event into the database
    statement = """INSERT INTO Event_Log VALUES({EVENT_NUMBER}, '{EVENT}');"""

    # Get the next event number by finding how many rows are already in the
    # database table
    event_num = len(db_view.execute('select * from Event_Log').fetchall()) + 1

    # Update the sql statement with the information
    statement = statement.replace('{EVENT_NUMBER}', str(event_num)).replace\
                ('{EVENT}', event)

    # Execute and commit the statement
    db_view.execute(statement)
    eventlog_db.commit()

    # Close the database
    db_view.close()
    eventlog_db.close()



# When the event logger checkbox is pressed, update the event log database
def event_logger_updated():
    if (log_events.get() == 1):
        event_logger('Event logging turned on')
    else:
        event_logger('Event logging turned off')



### Set up the GUI
# Window
archivist_window = Tk()
archivist_window.geometry('430x540')
archivist_window['bg'] = 'white'
archivist_window.title('News Archivist')

# Image
skynews = PhotoImage(file = getcwd() + r'\skynews.gif')
Label(archivist_window, image = skynews).grid(row = 1, column = 1, columnspan =\
                                              2)

# Label - status bar
status_bar = Label(archivist_window, text = 'Either pick a date to extract, or \
download the latest news', bg = 'white', pady = 5, font = ("Helvetica", 12))
status_bar.grid(row = 2, column = 1, columnspan = 2)


## Buttons
# Download button
download_button = Button(archivist_window, text = 'Download and archive the \
latest news', command = download_newspage)
download_button.config(width = 30)
download_button.grid(row = 3, column = 2)

# Extract button
extract_button = Button(archivist_window, text = 'Extract selected news from th\
e archive', command = extract_news)
extract_button.config(width = 30)
extract_button.grid(row = 4, column = 2)

# Display button
display_button = Button(archivist_window, text = 'Display the extracted news', \
                        command = display_extracted)
display_button.config(width = 30)
display_button.grid(row = 5, column = 2)


## Listbox - display and select dates for extract/displaying dates
# Inside a frame which also holds a scrollbar
listbox_frame = Frame(archivist_window)
scrollbar = Scrollbar(listbox_frame, orient = 'vertical')
dates_listbox = Listbox(listbox_frame, selectmode = 'single',
                        yscrollcommand = scrollbar.set, font =('Helvetica', 12)\
                        )

scrollbar.config(command=dates_listbox.yview)
# Pack scrollbar, listbox, and frame into window
scrollbar.pack(side = 'right', fill = 'y')
dates_listbox.pack(side = 'left', fill = 'both', expand=1)
listbox_frame.grid(row = 3, column = 1, rowspan = 4)

# Obtain list of previously extracted dates and insert it into dates_listbox
dates_list = listdir(getcwd() + r'\InternetArchive')

for date in dates_list:
    # Remove the .html from the date names and add a space to make the names
    # look nice
    date = date.strip('.html')
    new_date = date[:len(date) - 2] + ' ' + date[len(date) - 2:]

    # Insert the dates into the listbox
    dates_listbox.insert(END, new_date)
    # Check if today's news is already downloaded, if it is, disabled the
    # download button - this prevents the program from crashing if it tries
    # to download a file for a second time
    if (new_date == cur_date):
        download_button['state'] = 'disabled'


## Checkbutton - for logging events in Part B
global log_events
log_events = IntVar()
event_log_check = Checkbutton(archivist_window, text='Log events', var =log_events\
                           , bg = 'white', command = event_logger_updated)
event_log_check.grid(row = 6, column = 2)



