# Archivist

News archivist program developed in Python. Aimed to store top 10 news articles on a day, and be shown again at a later point in time.

**NOTE:**

*  Can no longer grab the top 10 articles. Due to a change in the way the data is shown by SkyNews, new articles don't have the same format. However using the already stored files will still allow you to view the summarised, and very basic web page with summaries of all news items, and links to the official news report.